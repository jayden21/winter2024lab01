import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
		for (int i =0; i<4; i++){
	
			if (word.charAt(i)==c){
			return i;
			}
			
		}
	    return -1;
	}
	public static char toUpperCase(char c) {
		c = Character.toUpperCase(c);
		return c;
	}
	/* method to display the letters that have been guessed
	* display the current state of the game 
	*
	* Ex: "bleh", false, true, false, true ---> display "_l_h"
	* Ex: "shin", true, false, false, true ---> display "s__n"
	*/
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		String newWord = ""; //current state of the game after each guess
		boolean[] letter = new boolean[] {letter0, letter1, letter2, letter3};
		//loop add the letter of Word to result if true, add space if false
		for (int i=0; i<letter.length; i++) {
			if (letter[i]) {
				newWord += word.charAt(i);
			} else {
				newWord += " _ ";
			}
		}
		System.out.println("Your result is " + newWord);
	}
	/*
	* method where the actual gameplay itself will be taken place
	* input the Word that player trying to guess
	* method will continously asks user to guess a letter
	*
	* stop running after player made 4 corrects
	* stop running after player made 6 misses
	*/
	public static void runGame(String word){
		Scanner reader = new Scanner (System.in);
		boolean[] letter = new boolean[] {false, false, false, false};
		int numberOfGuess = 0;
		
		while(numberOfGuess < 6 && !(letter[0] && letter[1] && letter[2] && letter[3])){
			System.out.println("Guess a letter");
			char guess = reader.next().charAt(0);
			guess =toUpperCase(guess);
			
			//loop checks if any character in the Word matches the guess word
			for (int i=0; i<4; i++){
				if(isLetterInWord(word, guess) == i) {
					letter[i]=true;
				}
			}
			//if statement increment the numberOfGuess if user guess wrong
			if (isLetterInWord(word, guess) == -1){
				numberOfGuess++;
				}
			printWork(word, letter[0], letter[1], letter[2], letter[3]);
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		if(letter[0] && letter[1] && letter[2] && letter[3]){
		System.out.println("Congrats! You got it :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}